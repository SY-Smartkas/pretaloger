# -*- coding: utf-8 -*-
from pymodbus.client.sync import ModbusTcpClient as ModbusClient
from pymodbus.payload import BinaryPayloadDecoder

client = ModbusClient("145.94.45.11", port=502)
client.connect()

# "tcpRegAddress": 512,
# "size": 2,
# "coding": "UINT32",
# "unit": "A",
# "multiplier": 0.01,
# "codingType": "UINT32_AB_CD",
# "functionCode": "READ_HOLDING_REGISTERS",
# "name": "Current L1",
# "byteOrder": "MSB",
# "wordOrder": "LSW"
res = client.read_holding_registers(512, count=2, unit=2)
print(res)
decoder = BinaryPayloadDecoder.fromRegisters(
    res.registers, byteorder=">", wordorder="<"
)
decoded = decoder.decode_32bit_uint()
print(decoded * 0.01)

# "tcpRegAddress": 514,
# "size": 2,
# "coding": "INT32",
# "unit": "W",
# "multiplier": 0.01,
# "codingType": "INT32_AB_CD",
# "functionCode": "READ_HOLDING_REGISTERS",
# "name": "Active Imported Power Total",
# "byteOrder": "MSB",
# "wordOrder": "LSW"
res = client.read_holding_registers(514, count=2, unit=2)
print(res)
decoder = BinaryPayloadDecoder.fromRegisters(
    res.registers, byteorder=">", wordorder="<"
)
decoded = decoder.decode_32bit_int()
print(decoded * 0.01)
