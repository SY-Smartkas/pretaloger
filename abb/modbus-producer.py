# -*- coding: utf-8 -*-
"""Connector between the ABB EQmatic Energy Analyzer and The Green Village digital platform.

The ABB module is read out via Modbus (recommended).
The data is sent to the Kafka using a Kafka producer (recommended).
"""

import logging
import os
import pathlib
import time
from configparser import ConfigParser

from abb_modbus import get_register_mapping, get_value, measurements
from confluent_kafka import SerializingProducer
from confluent_kafka.schema_registry import SchemaRegistryClient
from confluent_kafka.schema_registry.avro import AvroSerializer
from pymodbus.client.sync import ModbusTcpClient as ModbusClient

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)
logger = logging.getLogger(__file__)

if __name__ == "__main__":
    # Read config file.
    parent_dir = pathlib.Path(__file__).parent.resolve()
    config = ConfigParser()
    config.read(str(parent_dir / "pretaloger.cfg"))
    modbus_host = config.get("ABB", "host")
    modbus_port = config.get("ABB", "port")
    bootstrap_servers = config.get("Kafka", "bootstrap_servers")
    api_key = config.get("Kafka", "api_key")
    api_secret = config.get("Kafka", "api_secret")
    topic = config.get("Kafka", "topic")
    schema_registry_url = config.get("Kafka", "schema_registry_url")
    schema_registry_auth = config.get("Kafka", "schema_registry_auth")

    # Time between sensor readouts in seconds
    period = int(os.getenv("PERIOD", 1))
    logger.info("Period between data readout: %d seconds" % (period))

    conf = {"url": schema_registry_url, "basic.auth.user.info": schema_registry_auth}
    schema_registry_client = SchemaRegistryClient(conf)

    schema_str = '{"type":"record","name":"GreenVillageRecord","namespace":"thegreenvillage","fields":[{"name":"project_id","type":"string","doc":"Globally unique id for the project."},{"name":"application_id","type":"string","doc":"Unique id for the application/use case."},{"name":"device_id","type":"string","doc":"Unique id for the device."},{"name":"timestamp","type":"long","doc":"Timestamp (milliseconds since unix epoch) of the measurement."},{"name":"measurements","type":{"type":"array","items":{"type":"record","name":"measurement","fields":[{"name":"measurement_id","type":"string","doc":"Unique id for the measurement."},{"name":"value","type":["null","boolean","int","long","float","double","string"],"doc":"Measured value."},{"name":"unit","type":["null","string"],"doc":"Unit of the measurement.","default":null},{"name":"measurement_description","type":["null","string"],"doc":"Measurement description.","default":null}]}},"doc":"Array of measurements."},{"name":"project_description","type":["null","string"],"doc":"Project description.","default":null},{"name":"application_description","type":["null","string"],"doc":"Application/use case description.","default":null},{"name":"device_description","type":["null","string"],"doc":"Device description.","default":null},{"name":"device_manufacturer","type":["null","string"],"doc":"Device manufacturer.","default":null},{"name":"device_type","type":["null","string"],"doc":"Device type.","default":null},{"name":"device_serial","type":["null","string"],"doc":"Device serial number.","default":null},{"name":"location_id","type":["null","string"],"doc":"Unique id for the location.","default":null},{"name":"location_description","type":["null","string"],"doc":"Location description.","default":null},{"name":"latitude","type":["null","float"],"doc":"Latitude in decimal degrees of the device location (GPS coordinates).","default":null},{"name":"longitude","type":["null","float"],"doc":"Longitude in decimal degrees of the device location (GPS coordinates).","default":null},{"name":"altitude","type":["null","float"],"doc":"Altitude in meters above the mean sea level.","default":null}]}'
    conf = {"auto.register.schemas": False}
    avro_serializer = AvroSerializer(schema_registry_client, schema_str, conf=conf)

    # Create Producer instance
    producer = SerializingProducer(
        {
            "client.id": "pretaloger-abb-producer",
            "bootstrap.servers": bootstrap_servers,
            "sasl.mechanisms": "PLAIN",
            "security.protocol": "SASL_SSL",
            "sasl.username": api_key,
            "sasl.password": api_secret,
            # 'ssl.ca.location': '/etc/ssl/certs/ca-certificates.crt',
            "value.serializer": avro_serializer,
            "acks": "1",
            # If you don’t care about duplicates and ordering:
            "retries": 10000000,
            "delivery.timeout.ms": 2147483647,
            "max.in.flight.requests.per.connection": 5
            #        # If you don’t care about duplicates but care about ordering:
            #        'retries': 10000000,
            #        'delivery.timeout.ms': 2147483647,
            #        'max.in.flight.requests.per.connection': 1
            #        # If you care about duplicates and ordering:
            #        'retries': 10000000,
            #        'delivery.timeout.ms': 2147483647,
            #        'enable.idempotence': True
        }
    )

    delivered_records = 0

    # Optional per-message on_delivery handler (triggered by poll() or flush())
    # when a message has been successfully delivered or
    # permanently failed delivery (after retries).
    def acked(err, msg):
        global delivered_records
        """Delivery report handler called on
        successful or failed delivery of message
        """
        if err is not None:
            print("Failed to deliver message: {}".format(err))
        else:
            delivered_records += 1
            print(
                "Produced record to topic {} partition [{}] @ offset {}".format(
                    msg.topic(), msg.partition(), msg.offset()
                )
            )

    client = ModbusClient(modbus_host, port=modbus_port)
    register_mapping = get_register_mapping()

    try:
        while True:
            # Open or reconnect TCP to server.
            if not client.is_socket_open():
                if not client.connect():
                    logger.warning(f"Unable to connect to {modbus_host}:{modbus_port}")
                    time.sleep(5)
                    continue

            for slave in register_mapping:
                # FIXME: Skip the meters that are offline at the moment.
                if slave["slaveId"] == 1:
                    continue

                values = []
                for data_point in slave["dataPoints"]:
                    if not data_point["name"] in measurements[slave["name"]]:
                        continue

                    value = get_value(client, slave["address"], data_point)
                    logger.debug(value)
                    values.append(value)

                value = {
                    "project_id": "pretaloger",
                    "application_id": "abb",
                    "device_id": slave["name"],
                    "device_description": slave["name"],
                    "device_manufacturer": "ABB",
                    "device_type": slave["productName"],
                    "device_serial": slave["serialNumber"],
                    "timestamp": int(time.time() * 1e3),  # Millisecond timestamp
                    "measurements": values,
                }

                producer.produce(topic=topic, value=value, on_delivery=acked)
                # producer.poll(0)

            producer.poll(0)
            time.sleep(period)

    except KeyboardInterrupt:
        logger.error("Aborted by user")

    finally:
        producer.flush()

    print("{} messages were produced to topic {}!".format(delivered_records, topic))
