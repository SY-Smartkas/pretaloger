# -*- coding: utf-8 -*-
"""Connector between the ABB EQmatic Energy Analyzer and The Green Village digital platform.

The ABB module is read out by REST API calls which is less performant than Modbus.
The data is sent to the Kafka using a Kafka producer (recommended).
"""

import json
import logging
import os
import time
from configparser import ConfigParser

import requests
from abb_rest import device_measurements, get_abb_lists
from confluent_kafka import SerializingProducer
from confluent_kafka.schema_registry import SchemaRegistryClient
from confluent_kafka.schema_registry.avro import AvroSerializer
from requests.exceptions import RequestException

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)
logger = logging.getLogger(__file__)

if __name__ == "__main__":

    # Read config file.
    config = ConfigParser()
    config.read("pretaloger.cfg")
    abb_host = config.get("ABB", "url")
    abb_token = config.get("ABB", "token")
    bootstrap_servers = config.get("Kafka", "bootstrap_servers")
    api_key = config.get("Kafka", "api_key")
    api_secret = config.get("Kafka", "api_secret")
    topic = config.get("Kafka", "topic")
    schema_registry_url = config.get("Kafka", "schema_registry_url")
    schema_registry_auth = config.get("Kafka", "schema_registry_auth")

    # Time between sensor readouts in seconds
    period = int(os.getenv("PERIOD", 1))
    logger.info("Period between data readout: %d seconds" % (period))

    devices, device_datapoints = get_abb_lists(abb_host, abb_token)
    logger.info(f"Devices: {json.dumps(devices)}")

    conf = {"url": schema_registry_url, "basic.auth.user.info": schema_registry_auth}
    schema_registry_client = SchemaRegistryClient(conf)

    schema_str = '{"type":"record","name":"GreenVillageRecord","namespace":"org.thegreenvillage.record","fields":[{"name":"metadata","type":{"type":"record","name":"GreenVillageRecordMetadata","fields":[{"name":"project_id","type":"string","doc":"Project id: globally unique id for the project this device belongs to."},{"name":"device_id","type":"string","doc":"Device id: unique id for the device within a project."},{"name":"description","type":"string","doc":"General device description."},{"name":"type","type":["null","string"],"doc":"Type of the device."},{"name":"manufacturer","type":["null","string"],"doc":"Manufacturer of the device."},{"name":"serial","type":["null","string"],"doc":"Serial number of the device."},{"name":"placement_timestamp","type":["null","long"],"doc":"Timestamp (milliseconds since unix epoch, UTC) when the device was placed."},{"name":"location","type":["null","string"],"doc":"Description of the location the device is placed at."},{"name":"latitude","type":["null","float"],"doc":"Latitude in decimal degrees of the device location (GPS coordinates)."},{"name":"longitude","type":["null","float"],"doc":"Longitude in decimal degrees of the device location (GPS coordinates)."},{"name":"altitude","type":["null","float"],"doc":"Altitude in meters above the mean sea level."}]},"doc":"Metadata for the device."},{"name":"data","type":{"type":"record","name":"GreenVillageRecordData","fields":[{"name":"project_id","type":"string","doc":"Project id: globally unique id for the project this device belongs to."},{"name":"device_id","type":"string","doc":"Device id: unique id for the device within a project."},{"name":"timestamp","type":"long","doc":"Timestamp (milliseconds since unix epoch) of the measurement."},{"name":"values","type":{"type":"array","items":{"type":"record","name":"value","fields":[{"name":"name","type":"string"},{"name":"description","type":"string"},{"name":"unit","type":"string"},{"name":"type","type":{"type":"enum","name":"type","symbols":["NULL","BOOLEAN","INT","LONG","FLOAT","DOUBLE","STRING"]}},{"name":"value","type":["null","boolean","double","float","long","int","string"]}]}}}]},"doc":"Data from the device."}]}'
    conf = {"auto.register.schemas": False}
    avro_serializer = AvroSerializer(schema_str, schema_registry_client, conf=conf)

    # Create Producer instance
    producer = SerializingProducer(
        {
            "bootstrap.servers": bootstrap_servers,
            "sasl.mechanisms": "PLAIN",
            "security.protocol": "SASL_SSL",
            "sasl.username": api_key,
            "sasl.password": api_secret,
            "ssl.ca.location": "/etc/ssl/certs/ca-certificates.crt",
            "value.serializer": avro_serializer,
        }
    )

    delivered_records = 0

    # Optional per-message on_delivery handler (triggered by poll() or flush())
    # when a message has been successfully delivered or
    # permanently failed delivery (after retries).
    def acked(err, msg):
        global delivered_records
        """Delivery report handler called on
        successful or failed delivery of message
        """
        if err is not None:
            print("Failed to deliver message: {}".format(err))
        else:
            delivered_records += 1
            print(
                "Produced record to topic {} partition [{}] @ offset {}".format(
                    msg.topic(), msg.partition(), msg.offset()
                )
            )

    try:
        while True:
            for d in devices:
                # FIXME: Skip the meters that are offline at the moment.
                if d["primaryAddress"] != 2:
                    continue

                # Read out values.
                # The REST API returns values in the following format:
                # {
                #   "recordNumber": 219904,
                #   "value": 2359
                # }
                try:
                    # r = requests.get(f"{abb_host}/api/meters/{d['fingerPrint']}/values",
                    #                  headers={"Authorization": f"Longterm {abb_token}"},
                    r = requests.get(
                        "%s/api/meters/%d/values" % (abb_host, d["fingerPrint"]),
                        headers={"Authorization": "Longterm %s" % (abb_token)},
                        timeout=5,
                    )
                    logger.debug(r.text)
                    # logger.info(f"ABB /api/meters/{d['fingerPrint']}/values response code: {r.status_code}")
                    logger.info(
                        "ABB /api/meters/%s/values response code: %s"
                        % (d["fingerPrint"], r.status_code)
                    )
                    if r.status_code != 200:
                        continue
                    readings = r.json()["values"]

                except RequestException as e:
                    logger.error(e)
                    continue

                values = []
                # datapoints = next(filter(lambda x: x['fingerPrint'] == d['fingerPrint'], device_datapoints))
                # measurements = next(filter(lambda x: x['fingerPrint'] == d['fingerPrint'], device_measurements))
                datapoints = [
                    x for x in device_datapoints if x["fingerPrint"] == d["fingerPrint"]
                ][0]
                measurements = [
                    x
                    for x in device_measurements
                    if x["fingerPrint"] == d["fingerPrint"]
                ][0]
                for r in readings:
                    # datapoint = next(filter(lambda x: x['recordNumber'] == r['recordNumber'], datapoints['dataPoints']))
                    datapoint = [
                        x
                        for x in datapoints["dataPoints"]
                        if x["recordNumber"] == r["recordNumber"]
                    ][0]
                    if (
                        not datapoint["valueTypeDescription"]
                        in measurements["valueTypeDescriptions"]
                    ):
                        continue

                    value = {
                        "name": datapoint["valueTypeDescription"],
                        "description": datapoint["valueTypeDescription"],
                        "unit": datapoint["unit"],
                        "type": "FLOAT",
                        "value": r["value"] * datapoint["multiplier"],
                    }
                    logger.debug(value)
                    values.append(value)

                value = {
                    "metadata": {
                        "project_id": "pretaloger",
                        "device_id": measurements["device_id"],
                        "description": measurements["device_id"],
                        "manufacturer": d["manufacturer"],
                        "serial": d["serialNumber"],
                    },
                    "data": {
                        "project_id": "pretaloger",
                        "device_id": measurements["device_id"],
                        "timestamp": int(time.time() * 1e3),  # Millisecond timestamp
                        "values": values,
                    },
                }

                producer.produce(topic=topic, value=value, on_delivery=acked)
                # producer.poll(0)

            producer.poll(0)
            time.sleep(period)

    except KeyboardInterrupt:
        logger.error("Aborted by user")

    finally:
        producer.flush()

    print("{} messages were produced to topic {}!".format(delivered_records, topic))
